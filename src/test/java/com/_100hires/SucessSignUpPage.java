package com._100hires;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SucessSignUpPage extends WebDriverSettings{

    private WebDriver driver;
    private WebDriverWait wait;

    public SucessSignUpPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 5);
    }

    @FindBy(tagName = "h1")
    private WebElement title;

    private By confirmMessage = By.cssSelector(".check-email");

    String getTitle() {
        return this.title.getText();
    }

    String getConfirmMessage() {
        return this.driver.findElement(confirmMessage).getText();
    }

    void pageShouldBeLoaded() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(confirmMessage));
    }
}

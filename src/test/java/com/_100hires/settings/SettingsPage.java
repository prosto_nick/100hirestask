package com._100hires.settings;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SettingsPage {
    private WebDriver driver;
    private WebDriverWait wait;

    public SettingsPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 5);
    }

    private By teamInfo = By.cssSelector(".invite-form__title");

    @FindBy(css = "[href=\"/settings/company/team\"]")
    private WebElement team;

    public void selectTeam() {
        this.team.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(teamInfo));
    }

}

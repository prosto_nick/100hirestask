package com._100hires.settings;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TeamPage {
    private WebDriver driver;
    private WebDriverWait wait;

    public TeamPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 5);
    }

    @FindBy(css = ".form-control.invite-form__email")
    private WebElement emailField;

    @FindBy(css = ".btn-invite")
    private WebElement inviteButton;

    public void inviteMember(String email) {
        this.emailField.sendKeys(email);
        this.inviteButton.click();
    }

    public boolean teamMemberShouldBeInvited(String email) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(
                By.xpath("//td[contains(text(),\"" + email + "\")]")));
        return true;
    }



}

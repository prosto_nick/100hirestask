package com._100hires;

import com._100hires.candidates.CandidatesPage;
import com._100hires.candidates.NewCandidatePage;
import com._100hires.settings.SettingsPage;
import com._100hires.settings.TeamPage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.support.PageFactory;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestClass extends WebDriverSettings {

    @Test
    void signUpWithCorrectEmailAndRequiredInputFields() {

        StartPage startPage = PageFactory.initElements(driver, StartPage.class);
        startPage.open();

        Random random = new Random();
        int n = random.nextInt(1000) + 1;
        String email = "nyakovleva" + n + "@yandex.ru";

        startPage.fillEmail(email);
        startPage.pressSignUpButton();

        SignUpPage signUpPage = PageFactory.initElements(driver, SignUpPage.class);
        signUpPage.pageShouldBeLoaded();

        assertEquals("Add information",  signUpPage.getTitle());

        final String firstName = "Ivan";
        final String lastName = "Ivanov";

        signUpPage.fillFirstName(firstName);
        signUpPage.fillLastName(lastName);

        assertEquals(email, signUpPage.getEmailFromEmailField());

        signUpPage.pressFinishButton();

        SucessSignUpPage sucessSignUpPage = PageFactory.initElements(driver, SucessSignUpPage.class);
        sucessSignUpPage.pageShouldBeLoaded();

        assertEquals("Almost done!", sucessSignUpPage.getTitle());

        final String expectedConfirmMessage = "Confirm your email address: " + email + " change email";
        assertEquals(expectedConfirmMessage, sucessSignUpPage.getConfirmMessage());
    }

    @Test
    void signUpWithCorrectEmailWithoutRequiredInputFields() {

        StartPage startPage = PageFactory.initElements(driver, StartPage.class);
        startPage.open();

        Random random = new Random();
        int n = random.nextInt(1000) + 1;
        String email = "nyakovleva" + n + "@yandex.ru";

        startPage.fillEmail(email);
        startPage.pressSignUpButton();

        SignUpPage signUpPage = PageFactory.initElements(driver, SignUpPage.class);
        signUpPage.pageShouldBeLoaded();

        assertEquals("Add information",  signUpPage.getTitle());
        assertEquals( email, signUpPage.getEmailFromEmailField());

        signUpPage.pressFinishButton();

        assertEquals("First Name cannot be blank.", signUpPage.getBlankFistNameFieldMessage());
        assertEquals("Last Name cannot be blank.", signUpPage.getBlankLastNameFieldMessage());
    }

    @Test
    void signUpWithoutEmail() {

        StartPage startPage = PageFactory.initElements(driver, StartPage.class);
        startPage.open();
        startPage.pressSignUpButton();

        assertEquals( "Email cannot be blank.", startPage.getBlankEmailFieldLabel());
    }

    @Test
    void signUpWithIncorrectEmail() {

        StartPage startPage = PageFactory.initElements(driver, StartPage.class);
        startPage.open();

        Random random = new Random();
        int n = random.nextInt(1000) + 1;
        String email = "nyakovleva" + n + ".ru";

        startPage.fillEmail(email);
        startPage.pressSignUpButton();

        assertEquals("Email is not a valid email address.", startPage.getBlankEmailFieldLabel());
    }

    @Test
    void signInWithCorrectData() {

        signIn();

        MainPage mainPage = PageFactory.initElements(driver, MainPage.class);
        mainPage.pageShouldBeLoaded();

        assertEquals("Natalya Yakovleva", mainPage.getFullName());

    }

    @Test
    void inviteTeamMember() {
        signIn();

        MainPage mainPage = PageFactory.initElements(driver, MainPage.class);
        mainPage.pageShouldBeLoaded();
        mainPage.pressSettings();

        SettingsPage settingsPage = PageFactory.initElements(driver, SettingsPage.class);
        settingsPage.selectTeam();

        TeamPage teamPage = PageFactory.initElements(driver, TeamPage.class);

        Random random = new Random();
        int n = random.nextInt(1000) + 1;
        final String email = "test" + n + "@yandex.ru";
        teamPage.inviteMember(email);

        Assertions.assertTrue(teamPage.teamMemberShouldBeInvited(email));
    }

    @Test
    void addNewCandidate() {
        signIn();

        MainPage mainPage = PageFactory.initElements(driver, MainPage.class);
        mainPage.pageShouldBeLoaded();
        mainPage.pressCandidates();

        CandidatesPage candidatesPage = PageFactory.initElements(driver, CandidatesPage.class);
        candidatesPage.addNewCandidate();

        NewCandidatePage newCandidatePage = PageFactory.initElements(driver, NewCandidatePage.class);

        final String firstName = "Ivan";
        final String lastName = "Orlov";
        final String zipCode = "123456";
        final String phoneNumber = "886655";

        Random random = new Random();
        int n = random.nextInt(1000) + 1;
        final String email = "newCandidate" + n + "@mail.ru";

        newCandidatePage.fillFirstName(firstName);
        newCandidatePage.fillLastName(lastName);
        newCandidatePage.fillZipCode(zipCode);
        newCandidatePage.fillEmail(email);
        newCandidatePage.fillPhoneNumber(phoneNumber);
        newCandidatePage.pressSaveButton();

        String successTitle = firstName + " " + lastName + " assign jobs";
        Assertions.assertTrue(newCandidatePage.candidateShouldBeAddedTitle(successTitle));

    }

    void signIn() {
        StartPage startPage = PageFactory.initElements(driver, StartPage.class);
        startPage.open();
        startPage.signIn();

        LogInPage logInPage = PageFactory.initElements(driver, LogInPage.class);
        logInPage.pageShouldBeLoaded();
        logInPage.fillEmail("jack.nataly3@gmail.com");
        logInPage.fillPassword("test123456");
        logInPage.pressSignInButton();
    }
}

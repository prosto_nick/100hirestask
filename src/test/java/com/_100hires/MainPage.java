package com._100hires;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MainPage extends WebDriverSettings{

    private WebDriver driver;
    private WebDriverWait wait;

    public MainPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 5);
    }

    private By sidebarMenuUser = By.cssSelector(".sidebar-menu__user");
    private By companySelector = By.cssSelector(".settings-nav__item-link.settings-nav__item-link_active");
    private By candidatesSelector = By.cssSelector(".candidates-table__col-fixed.bg-th.candidates-head");

    @FindBy(css = ".sidebar-menu__user-title-row.sidebar-menu__user-title-row_name")
    private WebElement fullNameLabel;

    @FindBy(css = "a[href=\"/candidates\"]")
    private WebElement candidates;

    @FindBy(css = "a[href=\"/settings/company\"].sidebar-menu__item-link")
    private WebElement settings;

    public void pageShouldBeLoaded() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(sidebarMenuUser));
    }

    public String getFullName() {
        return this.fullNameLabel.getText();
    }

    void pressCandidates() {
        this.candidates.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(candidatesSelector));
    }

    void pressSettings() {
        this.settings.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(companySelector));
    }
}

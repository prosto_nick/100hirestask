package com._100hires;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.By;

public class SignUpPage {

    private WebDriver driver;
    private WebDriverWait wait;

    public SignUpPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 5);
    }

    @FindBy(tagName = "h1")
    private WebElement title;

    @FindBy(css = "input#fieldsform-first_name")
    private WebElement firstNameField;

    @FindBy(css="input#fieldsform-last_name")
    private WebElement lastNameField;

    @FindBy(css="input#fieldsform-email")
    private WebElement emailField;

    @FindBy(css="[type=\"submit\"]")
    private WebElement finishButton;

    private By blankFirstNameLabel = By.cssSelector(".field-fieldsform-first_name .help-block");

    private By blankLastNameLabel = By.cssSelector(".field-fieldsform-last_name .help-block");

    private By registerForm = By.id("register_form");

    void pageShouldBeLoaded() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(registerForm));
    }

    void fillFirstName(String firstName) {
        this.firstNameField.sendKeys(firstName);
    }

    void fillLastName(String lastName) {
        this.lastNameField.sendKeys(lastName);
    }

    void pressFinishButton() {
        this.finishButton.click();
    }

    String getTitle() {
        return this.title.getText();
    }

    String getEmailFromEmailField() {
        return this.emailField.getAttribute("value");
    }

    String getBlankFistNameFieldMessage() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(blankFirstNameLabel));
        return this.driver.findElement(blankFirstNameLabel).getText();
    }

    String getBlankLastNameFieldMessage() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(blankLastNameLabel));
        return this.driver.findElement(blankLastNameLabel).getText();
    }
}
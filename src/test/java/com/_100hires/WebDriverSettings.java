package com._100hires;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WebDriverSettings {

    public ChromeDriver driver;
    public WebDriverWait wait;

    @BeforeEach
    void setUp() {

        final String path = "/usr/local/bin/chromedriver";

        System.setProperty("webdriver.chrome.driver", path);
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 5);
    }

    @AfterEach
    void close() {
        driver.quit();
    }
}

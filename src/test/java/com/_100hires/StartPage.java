package com._100hires;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class StartPage extends WebDriverSettings{

    private WebDriver driver;
    private WebDriverWait wait;

    public StartPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 5);
    }

    void open() {
        driver.get("https://100hires.com/");
    }

    @FindBy(css = ".form-control#emailform-email")
    private WebElement email;

    @FindBy(css = ".signup-form__submit")
    private WebElement signUpButton;

    @FindBy(className = "header__top__login")
    private WebElement login;

    private By blankEmailFieldLabel = By.cssSelector("p.help-block.help-block-error");

    void fillEmail(String email) {
        this.email.sendKeys(email);
    }

    void pressSignUpButton() {
        this.signUpButton.click();
    }

    String getBlankEmailFieldLabel() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(blankEmailFieldLabel));
        return this.driver.findElement(blankEmailFieldLabel).getText();
    }

    void signIn() {
        this.login.click();
    }
}

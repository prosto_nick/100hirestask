package com._100hires;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LogInPage extends WebDriverSettings{

    private WebDriver driver;
    private WebDriverWait wait;

    public LogInPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 5);
    }

    private By loginForm = By.cssSelector("#login-form");

    @FindBy(css = "input#loginform-email")
    private WebElement emailField;

    @FindBy(css = "input#loginform-password")
    private WebElement passwordField;

    @FindBy(css = "button[type=\"submit\"]")
    private WebElement signInButton;

    void pageShouldBeLoaded() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(loginForm));
    }

    void fillEmail(String email) {
        this.emailField.sendKeys(email);
    }

    void fillPassword(String password) {
        this.passwordField.sendKeys(password);
    }

    void pressSignInButton() {
        this.signInButton.click();
    }
}
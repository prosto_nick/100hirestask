package com._100hires.candidates;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NewCandidatePage {
    private WebDriver driver;
    private WebDriverWait wait;

    public NewCandidatePage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 5);
    }

    @FindBy(css = "input#profileform-field_12529")
    private WebElement firstNameField;

    @FindBy(css = "input#profileform-field_12530")
    private WebElement lastNameField;

    @FindBy(css = "input#profileform-field_12531")
    private WebElement zipCodeField;

    @FindBy(css = "input#profileform-field_12601")
    private WebElement emailField;

    @FindBy(css = "input.phone-input")
    private WebElement phoneNumberField;

    @FindBy(css = "[value=\"Save\"]")
    private WebElement saveButton;

    public void fillFirstName(String firstName) {
        this.firstNameField.sendKeys(firstName);
    }

    public void fillLastName(String lastName) {
        this.lastNameField.sendKeys(lastName);
    }

    public void fillZipCode(String zipCode) {
        this.zipCodeField.sendKeys(zipCode);
    }

    public void fillEmail(String email) {
        this.emailField.sendKeys(email);
    }

    public void fillPhoneNumber(String phoneNumber) {
        this.phoneNumberField.sendKeys(phoneNumber);
    }

    public void pressSaveButton() {
        this.saveButton.click();
    }

    public boolean candidateShouldBeAddedTitle(String title) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(
                By.xpath("//h1[contains(text(),\"" + title + "\")]"))).isDisplayed();
        return true;
    }



}

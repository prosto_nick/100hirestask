package com._100hires.candidates;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CandidatesPage {
    private WebDriver driver;
    private WebDriverWait wait;

    public CandidatesPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 5);
    }

    private By newCandidatePage = By.cssSelector("form#w0");

    @FindBy(css = "[href=\"/candidate/default/create\"]")
    private WebElement newCandidateLink;

    public void addNewCandidate() {
        this.newCandidateLink.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(newCandidatePage));
    }
}
